"""QChem Calculator

Run qchem jobs.
"""

import numpy as np
from ase.calculators.calculator import Calculator, FileIOCalculator
from ase.calculators.calculator import ReadError, SCFError
from ase.calculators.calculator import all_changes
import ase.units

class QChem(FileIOCalculator):
    """
    QChem calculator
    """
    name = 'QChem'

    implemented_properties = ['energy', 'forces']
    command = 'qchem PREFIX.inp PREFIX.out'

    # Following the minimal requirements given in
    # http://www.q-chem.com/qchem-website/manual/qchem43_manual/sect-METHOD.html
    default_parameters = {'method': 'hf',
                          'basis': '6-31G*',
                          'jobtype': 'force',
                          'charge': 0}

    def __init__(self, restart=None, ignore_bad_restart_file=False,
                 label='qchem', scratch=None, np=1, nt=1, basisfile=None,
                 ecpfile=None, pbs=False, atoms=None, **kwargs):
        """
        Constructs a QChem-calculator object.
        """

        FileIOCalculator.__init__(self, restart, ignore_bad_restart_file,
                                  label, atoms, **kwargs)

        if restart is not None:
            try:
                self.read(restart)
            except ReadError:
                if ignore_bad_restart_file:
                    self.reset()
                else:
                    raise

        if pbs:
            self.command = 'qchem -pbs '
        else:
            self.command = 'qchem '
        if not np == 1:
            self.command += '-np %d '%np
        if not nt == 1:
            self.command += '-nt %d '%nt
        self.command += 'PREFIX.inp PREFIX.out'
        if not scratch == None:
            self.command += ' %s'%scratch

        self.basisfile = basisfile
        self.ecpfile = ecpfile

    def calculate(self, atoms=None, properties=['energy', 'energy'],
                  system_changes=all_changes):
        try:
            FileIOCalculator.calculate(self, atoms, properties, system_changes)
        except SCFError:
            old = self.parameters.pop('scf_guess', None)
            self.set(scf_guess='core')
            FileIOCalculator.calculate(self, atoms, properties, system_changes)
            self.set(scf_guess=old)

    def read(self, label):
        raise NotImplementedError

    def read_results(self):
        filename = self.label + '.out'

        with open(filename, 'r') as fileobj:
            lineiter = iter(fileobj)
            for line in lineiter:
                if 'SCF failed to converge' in line:
                    raise SCFError()
                elif 'ERROR: alpha_min' in line:
                    # Even though it is not technically a SCFError:
                    # raise SCFError
                    raise SCFError()
                elif ' Total energy in the final basis set =' in line:
                    convert = ase.units.Hartree
                    self.results['energy'] = float(line.split()[8]) * convert
                elif ' Gradient of SCF Energy' in line:
                    iforces = []
                    # Skip first line containing atom numbering
                    next(lineiter)
                    while True:
                        # Get next line and cut off the component numbering and
                        # remove trailing characters ('\n' and stuff)
                        line = next(lineiter)[5:].rstrip()
                        # Cut in chunks of 12 symbols and convert into strings
                        # This is prefered over string.split() as the fields may
                        # overlap when the gradient gets large
                        Fx = list(map(float,
                            [line[i:i+12] for i in range(0, len(line), 12)]))
                        # Repeat for Fy and Fz
                        line = next(lineiter)[5:].rstrip()
                        Fy = list(map(float,
                            [line[i:i+12] for i in range(0, len(line), 12)]))
                        line = next(lineiter)[5:].rstrip()
                        Fz = list(map(float,
                            [line[i:i+12] for i in range(0, len(line), 12)]))
                        iforces.extend(zip(Fx, Fy, Fz))

                        # After three force components we expect either a
                        # separator line, which we want to skip, or the end of
                        # the gradient matrix which is characterized by the
                        # line ' Max gradient component'.
                        # Maybe change stopping criterion to be independent of
                        # next line. Eg. if not lineiter.next().startswith('  ')
                        if ' Max gradient component' in next(lineiter):
                            # Minus to convert from gradient to force
                            convert = -ase.units.Hartree / ase.units.Bohr
                            self.results['forces'] = np.array(iforces) * convert
                            break

    def write_input(self, atoms, properties=None, system_changes=None):
        FileIOCalculator.write_input(self, atoms, properties, system_changes)
        filename = self.label + '.inp'

        with open(filename, 'w') as fileobj:
            fileobj.write('$comment\n   ASE generated input file\n$end\n\n')

            fileobj.write('$rem\n')
            for prm in self.parameters:
                if prm not in ['charge', 'multiplicity']:
                    fileobj.write('   %-25s   %s\n'%(prm, self.parameters[prm]))
            # Not even a parameters as this is an absolute necessity
            fileobj.write('   %-25s   %s\n'%('sym_ignore', 'true'))
            fileobj.write('$end\n\n')

            fileobj.write('$molecule\n')
            if ('multiplicity' not in self.parameters):
                tot_magmom = atoms.get_initial_magnetic_moments().sum()
                mult = tot_magmom + 1
            else:
                mult = self.parameters['multiplicity']
            fileobj.write('   %d %d\n'%(self.parameters['charge'], mult))
            for a in atoms:
                fileobj.write('   %s  %f  %f  %f\n'%(a.symbol, a.x, a.y, a.z))
            fileobj.write('$end\n\n')

            if not self.basisfile == None:
                with open(self.basisfile, 'r') as f_in:
                    basis = f_in.readlines()
                fileobj.write('$basis\n')
                fileobj.writelines(basis)
                fileobj.write('$end\n\n')

            if not self.ecpfile == None:
                with open(self.ecpfile, 'r') as f_in:
                    ecp = f_in.readlines()
                fileobj.write('$ecp\n')
                fileobj.writelines(ecp)
                fileobj.write('$end\n\n')

class QChemConvergenceMonitor(Calculator):
    """
    QChemConvergenceMonitor
    """
    name = 'QChemConvergenceMonitor'
    implemented_properties = QChem.implemented_properties
    default_parameters = {}

    def __init__(self, calculator):
        Calculator.__init__(self)
        if not isinstance(calculator, QChem):
            raise TypeError
        self.calculator = calculator

    def calculate(self, atoms, properties, system_changes):
        Calculator.calculate(self, atoms, properties, system_changes)

        # Remember previous scf_guess
        if 'scf_guess' in self.calculator.parameters:
            scf_guess = self.calculator.parameters['scf_guess']
        else:
            scf_guess = None
        # Try calculating
        try:
            results = [self.calculator.get_property(prop, atoms)
                       for prop in properties]
        except SCFError:
            if not (self.calculator.parameters['basis'].lower() == 'gen'
                and scf_guess == 'sad'):
                # If possible switch to scf_guess sad
                print('No SCF convergence, switching to scf_guess=sad')
                self.calculator.set(scf_guess='sad')
            else:
                # Else switch to scf_guess correct
                print('No SCF convergence, switching to scf_guess=core')
                self.calculator.set(scf_guess='core')
            # Retry:
            try:
                results = [self.calculator.get_property(prop, atoms)
                           for prop in properties]
            finally:
                # Reset scf_guess
                if scf_guess == None:
                    # Remove the key by using pop
                    self.calculator.parameters.pop('scf_guess', None)
                else:
                    self.calculator.set(scf_guess=scf_guess)

        self.results = dict(zip(properties, results))
